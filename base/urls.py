from django.conf.urls import url, include
from django.contrib import admin
from main import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/profile/$', views.ProfileView.as_view(), name='profile'),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^view/(?P<slug>[\w-]+)/$', views.TodoView.as_view()),
    url(r'^list/(?P<slug>[\w-]+)/$', views.ListView.as_view()),
    url(r'^add_task/$', views.AddTaskView.as_view(), name='add_task'),
    url(r'^change/$', views.ChangeTaskView.as_view(), name='change'),
    url(r'^share/(?P<signer>.+)/$', views.ShareView.as_view()),
    url(r'^$', views.index),
]
