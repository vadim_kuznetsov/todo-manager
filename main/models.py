# from django.db import models

from __future__ import unicode_literals
from django.utils import timezone


from django.db import models
from django.contrib.auth.models import User


class List(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(unique=True, null=False)

    def save(self, *args, **kwargs):
        super(List, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class Task(models.Model):
    CHOICES_STATUS = [('completed', 'Выполнено'), ('aborted', 'Не выполнено'), ('cancelled', 'Отменено')]

    title = models.CharField(max_length=100)
    desc = models.TextField(blank=True, null=True)
    created_date = models.DateField(auto_now=True)
    due_date = models.DateField(blank=True, null=True, default=timezone.now)
    completed_date = models.DateField(blank=True, null=True)
    list = models.ForeignKey(List, blank=True, null=True)
    status = models.CharField(max_length=100, choices=CHOICES_STATUS, blank=True)
    owner = models.ForeignKey(User, related_name='owner')
    slug = models.SlugField(unique=True, null=False)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.status and not self.completed_date:
            self.completed_date = timezone.now()
        super().save()
