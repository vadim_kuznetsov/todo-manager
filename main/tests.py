from django.test import TestCase, Client
from main.models import Task


class ViewTestCase(TestCase):
    fixtures = ['db_test.json']  # users, lists, items

    USER = 'test'
    PASSWORD = 'test_pass'

    def setUp(self):
        self.client = Client()
        self.logon = self.client.post('/accounts/login/', {'login': self.USER, 'password': self.PASSWORD})

    def test_login(self):  # TODO social
        self.assertNotEqual(self.logon.status_code, 404, 'Login page does not found.')
        self.assertNotEqual(self.client.get('/accounts/profile/').status_code, 404, 'Profile page does not exist.')
        self.assertRedirects(self.logon, '/accounts/profile/',
                             msg_prefix='Server does not accept the login and/or password.')

    def test_add_task(self):
        task_base_data = {'list': ['2'], 'title': ['Test title'], 'action': ['Подтвердить'],
                          'desc': ['Test desc'], 'status': ['completed'],
                          'due_date_day': ['18'], 'due_date_month': ['10'], 'due_date_year': ['2018']}
        self.client.post('/add_task/', task_base_data)
        self.assertGreater(len(Task.objects.filter(title='Test title')), 0, 'Task is not being added.')
        self.client.post('/add_task/', task_base_data)
