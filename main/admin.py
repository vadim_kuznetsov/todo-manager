from django.contrib import admin
from .models import Task, List


class ListAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


class ItemAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'list', 'due_date')
    list_filter = ('list',)
    ordering = ('due_date',)
    search_fields = ('name',)


admin.site.register(List, ListAdmin)
admin.site.register(Task, ItemAdmin)
