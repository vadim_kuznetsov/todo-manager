import itertools
from django import forms
from django.utils.text import slugify
from unidecode import unidecode

from .models import Task
from django.core import validators


class FormItem(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ('owner', 'slug')

    def save(self, commit=True):
        instance = super().save(commit=False)
        slug = slugify(unidecode(self.cleaned_data['title']))
        instance.slug = slug
        if Task.objects.filter(slug=slug):
            for count in itertools.count(1):
                if not Task.objects.filter(slug='{}-{}'.format(slug, count)):
                    instance.slug = '{}-{}'.format(slug, count)
                    break
        if commit:
            instance.save()
        return instance


class SendItem(forms.Form):
    email = forms.EmailField(validators=[validators.validate_email])
    cover_letter = forms.CharField(required=False)
