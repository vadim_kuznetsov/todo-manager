from django.views.generic import TemplateView
from django.shortcuts import redirect
from django.core import signing
from django.core.mail import send_mail
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import List, Task, User
from .forms import FormItem, SendItem


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = "profile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['todo_all'] = Task.objects.filter(owner=self.request.user).order_by('status', 'due_date')
        context['todo_lists'] = List.objects.filter()
        return context

    @staticmethod
    def post(request):
        Task.objects.get(id=request.POST.get('todo')).delete()
        return redirect('profile')


class ListView(LoginRequiredMixin, TemplateView):
    template_name = "list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list'] = List.objects.get(slug=kwargs['slug'])
        context['items'] = Task.objects.filter(owner=self.request.user, list_id=context['list'].id).order_by('status',
                                                                                                             'due_date')
        context['form'] = SendItem()
        context['todo_lists'] = List.objects.filter()
        return context

    def post(self, request, slug):
        if request.POST.get('submit') == 'Отправить':
            signer = signing.dumps({"user_id": self.request.user.id, "list_id": List.objects.get(slug=slug).id})
            send_mail(
                'Todo list',
                '{} \nhttp://127.0.0.1:8000/share/{} '.format(request.POST.get('cover_letter'), signer),
                'webmaster@localhost',
                [request.POST.get('email')],
                fail_silently=False)
        elif request.POST.get('submit').isdigit():
            Task.objects.get(id=request.POST.get('submit')).delete()
        return redirect(request.path)


class AddTaskView(LoginRequiredMixin, TemplateView):
    template_name = "add_task.html"

    def get_context_data(self):
        context = super().get_context_data()
        context['todo_lists'] = List.objects.filter()
        context['form'] = FormItem()
        return context

    def post(self, request):
        form = FormItem(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.owner = User.objects.get(id=request.user.id)
            task.save()
            return redirect('profile')
        else:
            context = super().get_context_data()
            context['todo_lists'] = List.objects.filter()
            context['form'] = form
            return super().render_to_response(context)


class ChangeTaskView(LoginRequiredMixin, TemplateView):
    template_name = "change.html"

    def get_context_data(self):
        context = super().get_context_data()
        task = Task.objects.get(id=self.request.GET['task'])
        context['task'] = task
        context['todo_lists'] = List.objects.filter()
        task.__dict__['list'] = task.__dict__['list_id']
        context['form'] = FormItem(initial=task.__dict__)
        return context

    def post(self, request):
        form = FormItem(request.POST)
        if form.is_valid():
            task = Task.objects.get(id=request.GET['task'])
            form = FormItem(request.POST, instance=task)
            form.save()
            return redirect('profile')
        else:
            context = super().get_context_data()
            context['todo_lists'] = List.objects.filter()
            context['form'] = form
            return super().render_to_response(context)


class TodoView(LoginRequiredMixin, TemplateView):
    template_name = "task.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['task'] = Task.objects.get(slug=kwargs['slug'])
        return context


class ShareView(TemplateView):
    template_name = "share.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        signer = signing.loads(kwargs['signer'])
        context['owner'] = User.objects.get(id=signer['user_id'])
        context['list'] = List.objects.get(id=signer['list_id'])
        context['items'] = Task.objects.filter(owner=signer['user_id'], list_id=signer['list_id']) \
            .order_by('status', 'due_date')
        return context


def index(request):
    if request.user.is_authenticated:
        return redirect('profile')
    else:
        return redirect('account_login')
